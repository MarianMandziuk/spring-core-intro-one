package com.and.company.sequence;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SignTable {
    private Sign signOne;
    private Sign signTwo;
    private Sign signThree;
    private Sign signFour;

    public SignTable(Sign signOne,
                     Sign signTwo,
                     Sign signThree,
                     Sign signFour) {
        this.signOne = signOne;
        this.signTwo = signTwo;
        this.signThree = signThree;
        this.signFour = signFour;
    }

    @Bean
    public SignTable createTable(Sign signOne,
                                 @Qualifier("beta") Sign signTwo,
                                 @Qualifier("gamma") Sign signThree,
                                 @Qualifier("delta") Sign signFour) {
        return new SignTable(signOne, signTwo, signThree, signFour);
    }

    @Override
    public String toString() {
        return "SignTable[" +
                "signOne=" + signOne +
                ", signTwo=" + signTwo +
                ", signThree=" + signThree +
                ", signFour=" + signFour +
                ']';
    }
}
