package com.and.company.sequence;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
@Qualifier("delta")
public class BeanDelta implements Sign {
    @Override
    public String toString() {
        return "Delta";
    }
}
