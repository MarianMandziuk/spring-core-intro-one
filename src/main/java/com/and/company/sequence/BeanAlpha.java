package com.and.company.sequence;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class BeanAlpha implements Sign {
    @Override
    public String toString() {
        return "Alpha";
    }
}
