package com.and.company.sequence;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier("beta")
public class BeanBeta implements Sign {
    @Override
    public String toString() {
        return "Beta";
    }
}
