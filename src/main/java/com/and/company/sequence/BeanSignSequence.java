package com.and.company.sequence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeanSignSequence {
    @Autowired
    private List<Sign> signs;

    public void printSigns() {
        for(Sign sign : signs) {
            System.out.println(sign);
        }
    }
}
