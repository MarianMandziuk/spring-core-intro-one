package com.and.company.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.and.company.injection", "com.and.company.other"})
public class ConfigInjection {
}
