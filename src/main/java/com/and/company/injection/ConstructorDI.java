package com.and.company.injection;

import com.and.company.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConstructorDI {
    private OtherBeanA otherBeanA;

    @Autowired
    public ConstructorDI(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }
}
