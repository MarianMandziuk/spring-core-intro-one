package com.and.company.injection;

import com.and.company.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FieldDI {
    @Autowired
    private OtherBeanC otherBeanC;
}
