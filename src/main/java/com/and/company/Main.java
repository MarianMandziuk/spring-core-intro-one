package com.and.company;

import com.and.company.config.ConfigInjection;
import com.and.company.config.ConfigOne;
import com.and.company.config.ConfigSequence;
import com.and.company.config.ConfigTwo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigOne.class,
                ConfigTwo.class,
                ConfigInjection.class,
                ConfigSequence.class);
        for (String beanDefinitionName : ctx.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
    }
}
